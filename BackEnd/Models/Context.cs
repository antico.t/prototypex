using Microsoft.EntityFrameworkCore;

namespace BackEnd.Models
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options) { }

        public DbSet<SampleEntity> SampleEntities { get; set; }
        public DbSet<User> Users { get; set; }
    }
}