using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BackEnd.Models
{
    public class SampleEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public double Quantity { get; set; }
        public bool CountForTotal { get; set; }
        public DateTime CreationDate { get; set; }
    }
}