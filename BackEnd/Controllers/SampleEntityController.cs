using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using BackEnd.Models;

namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SampleEntityController : ControllerBase
    {
        private readonly Context _context;

        public SampleEntityController(Context context)
        {
            _context = context;
        }

        // GET: api/SampleEntity
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SampleEntity>>> GetSampleEntities()
        {
            return await _context.SampleEntities.ToListAsync();
        }

        // GET: api/SampleEntity/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SampleEntity>> GetSampleEntity(string id)
        {
            var sampleEntity = await _context.SampleEntities.FindAsync(id);

            if (sampleEntity == null)
            {
                return NotFound();
            }

            return sampleEntity;
        }

        // PUT: api/SampleEntity/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSampleEntity(string id, SampleEntity sampleEntity)
        {
            if (id != sampleEntity.Id)
            {
                return BadRequest();
            }

            _context.Entry(sampleEntity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SampleEntityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SampleEntity
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<SampleEntity>> PostSampleEntity(SampleEntity sampleEntity)
        {
            _context.SampleEntities.Add(sampleEntity);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (SampleEntityExists(sampleEntity.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetSampleEntity", new { id = sampleEntity.Id }, sampleEntity);
        }

        // DELETE: api/SampleEntity/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<SampleEntity>> DeleteSampleEntity(string id)
        {
            var sampleEntity = await _context.SampleEntities.FindAsync(id);
            if (sampleEntity == null)
            {
                return NotFound();
            }

            _context.SampleEntities.Remove(sampleEntity);
            await _context.SaveChangesAsync();

            return sampleEntity;
        }

        private bool SampleEntityExists(string id)
        {
            return _context.SampleEntities.Any(e => e.Id == id);
        }
    }
}
