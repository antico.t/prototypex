# Prototipo per verifiche architetture in OCV reinforcement

## Obiettivo

L'obiettivo è quello di realizzare un'applicazione di test con lo scopo di testarne l'integrazione con l'ambiente esistente di OCV Besana.

## Architettura

L'architettura sarà strutturata in base alla seguente modellazione:

- Un server con installato Microsoft SQL Server
- Un BackEnd sviluppato con l'uso del framework ASP.NET Core 3.1
- Un applicativo FrontEnt sviluppato con l'uso del framework Angular 9

Il BackEnd, tramite l'utilizzo di Entity Framework, si occuperà di eseguire delle operazioni di Create, Read, Update e Delete con il database mentre il client avrà il classico ruolo di User Interface mostrando i dati ricevuti.

I dati trattati avranno una struttura di questo tipo:

*Sample Entity*

- Id: string
- Name: string
- Category: string
- Quantity: float
- CountForTotal: boolean
- CreationDate: DateTime

Sarà necessario creare una nuova istanza di database vuoto e dovrà essere fornita una stringa di connessione.
Sarà la stessa applicazione a creare le tabelle necessarie.

## Descrizione della soluzione

L'applicativo permetterà all'utente la visualizzazione di alcuni dati che potranno essere filtrati.
Inoltre sarà possibile inserire dei nuovi record o visionare alcuni dettagli per poterli modificare o cancellare.
L'app fornirà anche una sezione dedicata alla rappresentazione dei dati in oggetto sotto forma di grafici quali istogrammi o diagrammi a torta.

## Viste

L'applicativo si comporrà di 3 viste che si potranno raggiungere attraverso la navigazione del menù principale o tramite l'interazione con vari oggetti tipo bottoni.

### Vista principale: Master

In questa pagina verrà visualizzata una griglia che mostrarà l'elenco dei dati aventi la struttura descritta in precedenza.
Saranno presenti due campi che permetteranno il filtraggio dei dati in tabella.
Per ogni record presente ci sarà un bottone la cui funzione sarà quella di navigare alla vista di dettaglio.
Un bottone in basso a destra consentirà la navigazione ad una nuova pagina per l'inserimento di un nuovo record.

### Vista di dettaglio: Data Detail

In questa vista si vedrà il dettaglio di un singolo record e, premendo sull'icona a forma di matita, i campi verranno abilitati alla modifica per permettere l'aggiornamento dei vari dati.
In basso a destra un'icona a forma di cestino cancellerà l'intero record in oggetto.

### Vista grafici: Graph

In questa sezione sarà presente un menù a tendina dal quale l'utente potrà scegliere il tipo di grafico da visualizzare:
- Istogramma
- Grafico a torta

Una chckbox avrà la funzione di includere/escludere nella visualizzazione grafica i record con CountForTotal true/false.

## Stime

La realizzazione del progetto in tutte le sue parti è stimata per 4 giorni lavorativi con un margine di +/- 8 ore includendo la fase di installazione.
