import { Component, ViewChild, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { ShellService, ShellData } from './services/shell.service';
import { LoginService } from './services/login.services';
import { Subscription } from 'rxjs';
import { User } from './models/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy, ShellData {
  @ViewChild('title') title: ElementRef;
  user: User;
  sub: Subscription;

  constructor(
    private ss: ShellService,
    private ls: LoginService,
    private r: Router
  ) { }

  ngOnInit() {
    this.ss.register(this);
    this.sub = this.ls.user.subscribe(u => {
      if (u) {
        this.user = u;
        this.r.navigateByUrl('/master');
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
