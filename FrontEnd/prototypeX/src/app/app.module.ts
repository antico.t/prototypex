import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { MasterComponent } from './components/master/master.component';
import { ShellComponent } from './components/shell/shell.component';
import { ShellContentDirective } from './components/shell/shell-content.directive';
import { ShellMenuDirective } from './components/shell/shell-menu.directive';
import { DetailComponent } from './components/detail/detail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './components/login/login.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    MasterComponent,
    ShellComponent,
    ShellContentDirective,
    ShellMenuDirective,
    DetailComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  exports: [
    MaterialModule,
    ShellComponent,
    ShellContentDirective,
    ShellMenuDirective
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
