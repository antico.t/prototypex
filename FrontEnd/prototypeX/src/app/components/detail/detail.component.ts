import { Component, OnInit } from '@angular/core';
import { SampleEntity } from 'src/app/models/sample-entity';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap, map, filter, distinctUntilChanged, debounceTime } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SampleEntityService } from 'src/app/services/sample-entity.service';
import { LoginService } from 'src/app/services/login.services';

function newProductForm() {
  return {
    name: '',
    category: '',
    quantity: 0,
    countForTotal: true,
    creationDate: new Date()
  };
}

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  product = new SampleEntity();
  productForm = this.fb.group(newProductForm());
  loading = false;

  constructor(
    private fb: FormBuilder,
    private a: ActivatedRoute,
    private r: Router,
    private sb: MatSnackBar,
    private ps: SampleEntityService,
    public ls: LoginService
  ) { }

  ngOnInit(): void {
    this.productForm.disable();

    this.a.paramMap.pipe(
      switchMap(pmap => this.ps.entities.pipe(
        map(pl => pl.find(p => p.id == pmap.get('id'))),
        filter(p => !!p),
        distinctUntilChanged()
      ))
    ).subscribe(p => {
      this.productForm.reset(newProductForm(), { emitEvent: false });
      this.productForm.patchValue(this.product = p, { emitEvent: false });
    });

    this.productForm.valueChanges.pipe(
      debounceTime(200),
      distinctUntilChanged()
    ).subscribe(() => {
      Object.assign(this.product, this.productForm.value);
      this.ps.put(this.product);
    });
  }

  async remove() {
    this.loading = true;
    await this.ps.delete(this.product);
    this.loading = false;

    this.sb.open('Product deleted', 'Ok', { duration: 5000 });
    this.back();
  }

  back() {
    this.r.navigate(['master']);
  }
}
