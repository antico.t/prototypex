import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { SampleEntity } from 'src/app/models/sample-entity';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SampleEntityService } from 'src/app/services/sample-entity.service';
import { Subscription } from 'rxjs';
import { LoginService } from 'src/app/services/login.services';

@Component({
  selector: 'app-master',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.css']
})
export class MasterComponent implements OnInit, OnDestroy {
  dataSource = new MatTableDataSource<SampleEntity>();
  loading = false;
  sub: Subscription;

  selectedRowIndex = -1;

  displayedColumns: string[] = [
    'name',
    'category',
    'quantity',
    'countForTotal',
    'creationDate',
    'detail'
  ];

  constructor(
    private r: Router,
    private sb: MatSnackBar,
    private es: SampleEntityService,
    public ls: LoginService
  ) { }

  async ngOnInit() {
    this.sub = this.es.entities.subscribe(e => {
      this.dataSource.data = e;
    });

    this.loading = true;
    await this.es.get();
    this.loading = false;
  }

  async addProduct() {
    const se = new SampleEntity({
      name: 'New products'
    });

    this.loading = true;
    await this.es.post(se);
    this.loading = false;

    this.sb.open('Product inserted', 'Ok', { duration: 5000 });
    this.r.navigate(['detail', se.id]);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  highlight(row) {
    this.selectedRowIndex = row.id;
  }
}
