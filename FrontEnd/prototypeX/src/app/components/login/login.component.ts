import { Component, OnInit, HostListener } from '@angular/core';
import { LoginService } from 'src/app/services/login.services';
import { User } from 'src/app/models/user';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  user: User = new User();
  logging = false;

  constructor(
    private ls: LoginService,
    private sb: MatSnackBar
  ) { }

  @HostListener('window:keydown.enter')
  login() {
    this.logging = true;

    this.ls.login(this.user).then(u => {
      this.sb.open('Successfully logged in', 'Ok', { duration: 5000 });
    }).catch(e => {
      this.sb.open('Login failed', 'Ok', { duration: 5000 });
      console.log('Errore: ', e);
    }).finally(() => { this.logging = false; });
  }
}
