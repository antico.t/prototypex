import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject, Observable, combineLatest, BehaviorSubject } from 'rxjs';
import { switchMap, startWith, scan, shareReplay, sample } from 'rxjs/operators';
import { SampleEntity } from '../models/sample-entity';

@Injectable({
  providedIn: 'root'
})
export class SampleEntityService {
  readonly url = 'api/SampleEntity';
  private entitiesSub = new BehaviorSubject<SampleEntity[]>([]);
  entities = this.entitiesSub.asObservable();

  constructor(private h: HttpClient) { }

  async get(): Promise<SampleEntity[]> {
    const entities = (await this.h.get<SampleEntity[]>(this.url).toPromise()).map(e => new SampleEntity(e));
    this.entitiesSub.next(entities);
    return entities;
  }

  async post(se: SampleEntity): Promise<void> {
    const entities = this.entitiesSub.value;
    entities.push(se);

    const { id, ...rest } = await this.h.post<SampleEntity>(this.url, se).toPromise();

    if (!entities.includes(se)) {
      await this.delete(se);
    } else {
      se.id = id;

      if (Object.keys(rest).some(key => {
        se[key] = se[key] || rest[key];
        return se[key] != rest[key];
      })) {
        await this.put(se);
      }
    }
  }

  async put(se: SampleEntity): Promise<void> {
    this.emit();

    if (se.id) {
      await this.h.put(`${this.url}/${se.id}`, se).toPromise();
    }
  }

  async delete(se: SampleEntity): Promise<void> {
    const entities = this.entitiesSub.value;
    const i = entities.indexOf(se);
    entities.splice(i, +(i > -1));

    if (se.id) {
      await this.h.delete(`${this.url}/${se.id}`).toPromise();
    }
  }

  private emit() {
    this.entitiesSub.next(this.entitiesSub.value);
  }
}
