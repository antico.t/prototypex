import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

const url = '/api/User/LogIn';

@Injectable({
  providedIn: 'root'
})

export class LoginService {
  private userSub = new BehaviorSubject<User>(null);
  user = this.userSub.asObservable();

  constructor(private h: HttpClient) { }

  async login(u: User): Promise<User> {
    let user = await this.h.post<User>(url, u).toPromise();
    user = new User(user);

    this.userSub.next(user);

    return user;
  }

  isRole(r: string): Observable<boolean> {
    return this.user.pipe(map(u => u.role == r));
  }
}
