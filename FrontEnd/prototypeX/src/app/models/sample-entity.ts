export class SampleEntity {
  id?: string;
  name?: string;
  category?: string;
  quantity?: number;
  countForTotal?: boolean;
  creationDate?: Date;

  constructor(data?: Partial<SampleEntity>) {
    Object.assign(this, data);
    this.creationDate = this.creationDate && new Date(this.creationDate);
  }
}
