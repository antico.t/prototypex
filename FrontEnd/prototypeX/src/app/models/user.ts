export class User {
  id?: string;
  email?: string;
  password?: string;
  role?: string;

  constructor(data?: Partial<User>) {
    Object.assign(this, data);
  }
}
